package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private TextInputLayout LoginEmail;
    private TextInputLayout LoginPassword;
    private Button Signup;
    public String Signup_password;
    public String Signup_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoginEmail = findViewById(R.id.input_email);
        LoginPassword = findViewById(R.id.input_password);
        Signup = findViewById(R.id.sign_up);
        Intent intent = getIntent();
        Signup_password = intent.getStringExtra(activity_main2.sent_password);
        Signup_email = intent.getStringExtra(activity_main2.sent_email);

        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, activity_main2.class);
                startActivity(intent);
            }
        });

    }

    private boolean ValidateEmail() {
        String LoginEmailInput = LoginEmail.getEditText().getText().toString().trim();
        if (LoginEmailInput.isEmpty()) {
            LoginEmail.setError("Field Can't be empty");
            return false;
        }

         else if (!(LoginEmailInput.equals(Signup_email))) {
            LoginEmail.setError("invalid Email Address");
            return false;
        } else {
            LoginEmail.setError(null);
            LoginEmail.setErrorEnabled(false);
            return true;
        }

    }

    private boolean ValidatePassword() {

        String LoginpasswordInput = LoginPassword.getEditText().getText().toString().trim();
        if (LoginpasswordInput.isEmpty()) {
            LoginPassword.setError("Field Can't be empty");
            return false;
        }
        else if (!(LoginpasswordInput.equals(Signup_password))) {
            LoginPassword.setError("Incorrect password");
            return false;
        } else {
            LoginPassword.setError(null);
            LoginPassword.setErrorEnabled(false);
            return true;
        }

    }

    public void confirmInput(View v) {
        if (!ValidateEmail() | !ValidatePassword()) {
            return;
        }

        Intent intent = new Intent(MainActivity.this, MainActivity3.class);
        startActivity(intent);
    }

}

package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication1.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class activity_main2 extends AppCompatActivity {
    public static final String sent_password;

    static {
        sent_password = "com.example.myapplication1.sent_password";
    }

    public static final String sent_email;

    static {
        sent_email = "com.example.myapplication1.sent_email";
    }

    private TextInputLayout getsignup_Firstname;
    private TextInputLayout getsignup_Seondname;
    private TextInputLayout getsignup_email;
    public TextInputLayout getsignup_password;
    private TextInputLayout getsignup_confirmpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getsignup_Firstname = findViewById(R.id.input_fullname);
        getsignup_Seondname = findViewById(R.id.input_lastname);
        getsignup_email = findViewById(R.id.input_email_signup);
        getsignup_password = findViewById(R.id.input_password_signup);
        getsignup_confirmpassword= findViewById(R.id.input_confirmpassword_signup);
    }

    private boolean ValidateFullname() {
        String FirtName = Objects.requireNonNull(getsignup_Firstname.getEditText()).getText().toString().trim();
        if (FirtName.isEmpty()) {
            getsignup_Firstname.setError("Field Can't be empty");
            return false;
        } else {
            getsignup_Firstname.setError(null);
            getsignup_Firstname.setErrorEnabled(false);
            return true;
        }

    }

    private boolean ValidateLastname() {
        String LastName = Objects.requireNonNull(getsignup_Seondname.getEditText()).getText().toString().trim();
        if (LastName.isEmpty()) {
            getsignup_Seondname.setError("Field Can't be empty");
            return false;
        } else {
            getsignup_Seondname.setError(null);
            getsignup_Seondname.setErrorEnabled(false);
            return true;
        }

    }

    protected boolean ValidateEmailSignUp() {
        String LoginEmailSignUP = Objects.requireNonNull(getsignup_email.getEditText()).getText().toString().trim();
        if (LoginEmailSignUP.isEmpty()) {
            getsignup_email.setError("Field Can't be empty");
            return false;
        }
        if (!(LoginEmailSignUP.matches("^(?=.*[@])(?=\\S+$).{8,}$")))
        {
            getsignup_email.setError("Your email address must have @  ");
            return false;
        }
        else {
            getsignup_email.setError(null);
            getsignup_email.setErrorEnabled(false);
            return true;
        }

    }


    public boolean ValidatePasswordSignUp() {
        String PasswordSignUp = Objects.requireNonNull(getsignup_password.getEditText()).getText().toString().trim();
        if (PasswordSignUp.isEmpty()) {
            getsignup_password.setError("Field Can't be empty");
            return false;
        } else if (!(PasswordSignUp.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"))) {
            getsignup_password.setError("mush have one lower,upper case letter, special character And 8 letter atleast");
            return false;
        } else {
            getsignup_password.setError(null);
            getsignup_password.setErrorEnabled(false);
            return true;
        }

    }


    protected boolean ValidateConfirmPassword() {
        String PasswordSignUp = Objects.requireNonNull(getsignup_password.getEditText()).getText().toString().trim();
        String ConfirmPassword = Objects.requireNonNull(getsignup_confirmpassword.getEditText()).getText().toString().trim();
        if (ConfirmPassword.isEmpty()) {
            getsignup_confirmpassword.setError("Field Can't be empty");
            return false;
        }
        if (!(ConfirmPassword.equals(PasswordSignUp))) {
            getsignup_confirmpassword.setError("Does Not Match");
            return false;
        } else {
            getsignup_confirmpassword.setError(null);
            getsignup_confirmpassword.setErrorEnabled(false);
            return true;
        }

    }


    public void ConfirmSignUp(View v) {
        if (!ValidateFullname() | !ValidateLastname() | !ValidateEmailSignUp() |
                !ValidateConfirmPassword() | !ValidatePasswordSignUp()) return;
        Intent intent;
        intent = new Intent(activity_main2.this, MainActivity.class);

        String PasswordSignUp = Objects.requireNonNull(getsignup_password.getEditText()).getText().toString().trim();
        String EmailSignUP = Objects.requireNonNull(getsignup_email.getEditText()).getText().toString().trim();

        intent.putExtra(sent_email, EmailSignUP);
        intent.putExtra(sent_password, PasswordSignUp);
        startActivity(intent);
    }
}